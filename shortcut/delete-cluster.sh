#!/bin/bash

for f in "$1"*.yml; 
do
	kubectl delete -f "$f"
done
