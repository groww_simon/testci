#!/bin/bash

for f in "$1"*.yml; 
do
	kubectl create -f "$f"
done
