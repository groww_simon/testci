#!/bin/bash

# Delete disks created for the CI
gcloud compute disks delete sonarqube-disk
gcloud compute disks delete android-sdk-disk
