#!/bin/bash
# Create the needed disks for persistence

export ZONE=europe-west1-d

gcloud compute disks create --size=10GB --zone=$ZONE sonarqube-disk
gcloud compute disks create --size=50GB --zone=$ZONE android-sdk-disk
