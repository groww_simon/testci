#!/bin/bash

#Populate the disk containing most needed sdks
#Warning : the disk must be created but not mounted on any instances !
export INSTANCE=instance-2
export DISK=android-sdk-disk
export MOUNT_POINT=mount_point

export ANDROID_SDK_FILENAME=android-sdk_r24.3.4-linux.tgz 
export ANDROID_SDK_URL=http://dl.google.com/android/$ANDROID_SDK_FILENAME
# export ANDROID_API_LEVELS=android-8,android-14,android-16,android-21,android-22,android-23
# export ANDROID_BUILD_TOOLS_VERSION=build-tools-23.0.1,build-tools-22.0.1

cd ~
mkdir -p $MOUNT_POINT
gcloud compute instances attach-disk $INSTANCE --disk $DISK
sudo /usr/share/google/safe_format_and_mount -m "mkfs.ext4 -F" /dev/disk/by-id/google-persistent-disk-1 $MOUNT_POINT

sudo chmod -R 777 $MOUNT_POINT
cd $MOUNT_POINT

if [ ! -d "./tools/" ]; then
	wget $ANDROID_SDK_URL
	tar xf $ANDROID_SDK_FILENAME --strip 1 && rm -f $ANDROID_SDK_FILENAME
fi

echo y | ./tools/android update sdk --no-ui
sudo umount $MOUNT_POINT
gcloud compute instances detach-disk $INSTANCE --disk $DISK


